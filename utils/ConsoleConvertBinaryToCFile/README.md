Simple utility to covert a binary to .c file:
* This will convert all 0xFF to 0xFE, otherwise the program will not download
* Use VS Desktop 2013 if you need to edit
* No error checking at all
* By default it will convert the file "binary.bin" to "source.c", edit program to update