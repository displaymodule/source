﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleConvertBinaryToCFile
{
   class Program
   {
      static string filePath = @"binary.bin";
      static string outputFilename = @"source.c";

      static void Main(string[] args)
      {
         if (!File.Exists(filePath))
         {
            Console.WriteLine("File "+filePath+" does not exists!");
            Environment.Exit(1);
         }

         byte[] fileBytes = File.ReadAllBytes(filePath);
         StringBuilder sb = new StringBuilder();

         FileInfo fileInfo = new FileInfo(filePath);

         // Apply header
         sb.Append("/********************************************************\n");
         sb.Append("File name:       "+fileInfo.Name+"\n");
         sb.Append("File size:       "+fileBytes.Length+"\n");
         sb.Append("Convertion date: "+DateTime.Now+"\n\n");
         sb.Append("File generated using DisplayModule's binaryconverter\n");
         sb.Append("********************************************************/\n\n");

         sb.Append("#include <avr/pgmspace.h>\n\n");
         sb.Append("const prog_uchar dmlogo[0x" + fileBytes.Length.ToString("X") + "] PROGMEM ={");

         for (int i = 0; i < fileBytes.Length; i++)
         {
            if (i % 32 == 0)
            {
               sb.Append("\n  ");
            }

            if (fileBytes[i] == 0xFF) // Workaround for Arudino bug
            {
               sb.Append("0xFE");
            }
            else
            {
               sb.Append("0x" + fileBytes[i].ToString("X2"));
            }

            if (i != (fileBytes.Length - 1))
            {
               sb.Append(", ");
            }

         }

         sb.Append("\n};");
         File.WriteAllText(outputFilename, sb.ToString());
      }
   }
}
