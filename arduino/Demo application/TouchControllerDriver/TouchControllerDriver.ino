// Touch Demo without using IRQ (to save one pin)
// Tested with Xpt2046

#define T_CLK   15
#define T_MOSI  14 // T_DIN
#define T_CS    8
#define T_MISO  9  // T_DO
// #define T_IRQ   10    

unsigned int TI_X, TI_Y;

void SpiStart() 
{
  digitalWrite(T_CS, HIGH);
  digitalWrite(T_CLK, HIGH);
  digitalWrite(T_MOSI, HIGH);
  digitalWrite(T_CLK, HIGH);
}

void WriteCharToTouch(unsigned char num) {
  unsigned char count=0;
  unsigned char temp;
  unsigned nop;
  temp=num;
  digitalWrite(T_CLK,LOW);
  for(count=0;count<8;count++) {
    if(temp&0x80) {
      digitalWrite(T_MOSI,HIGH);
    }
    else {
      digitalWrite(T_MOSI,LOW);
    }
    
    temp=temp<<1; 

    digitalWrite(T_CLK,LOW);                
    nop++;
    nop++;
    digitalWrite(T_CLK,HIGH);
    nop++;
    nop++;
  }
}

unsigned int ReadCharFromTouch() { 
  unsigned nop;
  unsigned char count=0;
  unsigned int Num=0;
  for(count=0;count<12;count++) {
    Num<<=1;
    digitalWrite(T_CLK,HIGH);//DCLK=1; _nop_();_nop_();_nop_();                
    nop++;
    digitalWrite(T_CLK,LOW);//DCLK=0; _nop_();_nop_();_nop_();
    nop++;
    if(digitalRead(T_MISO)) {
      Num++;
    }
  }
  return(Num);
}

bool ReadTouchData(void) {
  unsigned int TP_X, TP_Y;
  
  digitalWrite(T_CS, LOW);                  
  WriteCharToTouch(0x90);
  digitalWrite(T_CLK, HIGH);
  digitalWrite(T_CLK, LOW);
  TP_Y = ReadCharFromTouch();
  WriteCharToTouch(0xD0);      
  digitalWrite(T_CLK, HIGH);
  digitalWrite(T_CLK, LOW);
  TP_X = ReadCharFromTouch();
  digitalWrite(T_CS, HIGH);
  
  Serial.print("RAW X: ");
  Serial.println(TP_X);
  Serial.print("RAW Y: ");
  Serial.println(TP_Y);
  
  // This you get by measuring the highest and lowest RAW value for X and Y
  // Example:
  // 1. highX - lowX = deltaX               // 3880 - 220 = 3660
  // 2. deltaX / screenWidth = modifierX    // 3660 / 240 = 15.25
  // Formula: TP_X-220/15.25
  
  // DM-TFT24-104
  TI_X=240-((TP_X-210)/15);
  TI_Y=320-((TP_Y-280)/11.24);
  
  // DM-TFT28-103
  //TI_X=((TP_X-220)/15.25);
  //TI_Y=((TP_Y-108)/11.16);
  
  if (TI_X >= 0 && TI_X <= 240 && TI_Y >= 0 && TI_Y <= 320)
  {
     return true;
  }
  return false;
}

void setup() {
  Serial.begin(9600);
  
  unsigned char port;
  for(port=0; port<20; port++)
  {
    pinMode(port, OUTPUT);
  }
  pinMode(T_MISO, INPUT);
//  pinMode(T_IRQ, INPUT);
  SpiStart();
}

void loop() {
//  if (digitalRead(T_IRQ) == 0) {
//      ReadTouchData();
  if (ReadTouchData()) {
      Serial.print("X: ");
      Serial.println(TI_X);
      Serial.print("Y: ");
      Serial.println(TI_Y);
  }
  delay(250);
}
